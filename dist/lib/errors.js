"use strict";

var _util = require('./util');

function UsageError(msg) {
  if (!(this instanceof UsageError)) return new UsageError(msg);
  var tmp = Error.call(this, msg);
  (0, _util.ownAssign)(this, tmp);
}
UsageError.prototype = Object.create(Error.prototype);
UsageError.prototype.name = 'UsageError';
Object.assign(UsageError, {
  needAddressThenAmount: function needAddressThenAmount() {
    return UsageError('must supply target address then amount');
  },
  invalidAddress: function invalidAddress(addr) {
    return UsageError('invalid address ' + addr);
  },
  nonexistentAccount: function nonexistentAccount(idx) {
    return UsageError('nonexistent Ethereum account at index ' + idx);
  }
});

module.exports = {
  UsageError: UsageError
};
//# sourceMappingURL=errors.js.map
