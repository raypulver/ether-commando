"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _nodeGetopt = require('node-getopt');

var _nodeGetopt2 = _interopRequireDefault(_nodeGetopt);

var _path = require('path');

var _os = require('os');

var _lodash = require('lodash');

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

var _bigNumber = require('big-number');

var _bigNumber2 = _interopRequireDefault(_bigNumber);

var _log = require('./log');

var _log2 = _interopRequireDefault(_log);

var _errors = require('./errors');

var _validation = require('./validation');

var _util = require('./util');

var _util2 = require('util');

var _url = require('url');

var _pathExists = require('path-exists');

var _pathExists2 = _interopRequireDefault(_pathExists);

var _mkdirp = require('mkdirp');

var _mkdirp2 = _interopRequireDefault(_mkdirp);

var _fs = require('fs');

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

var _async = require('async');

var _readlineSync = require('readline-sync');

var _readlineSync2 = _interopRequireDefault(_readlineSync);

var _ethLightwallet = require('eth-lightwallet');

var _ethLightwallet2 = _interopRequireDefault(_ethLightwallet);

var _hookedWeb3Provider = require('hooked-web3-provider');

var _hookedWeb3Provider2 = _interopRequireDefault(_hookedWeb3Provider);

var _sprintf = require('sprintf');

var _sprintf2 = _interopRequireDefault(_sprintf);

var _mathjs = require('mathjs');

var _mathjs2 = _interopRequireDefault(_mathjs);

var _escapeRegexp = require('escape-regexp');

var _escapeRegexp2 = _interopRequireDefault(_escapeRegexp);

var _ethereumjsTx = require('ethereumjs-tx');

var _ethereumjsTx2 = _interopRequireDefault(_ethereumjsTx);

var _ethereumjsUtil = require('ethereumjs-util');

var _sourceMapSupport = require('source-map-support');

var _ethereumjsWallet = require('ethereumjs-wallet');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _sourceMapSupport.install)();

var fmtStr = '  %-35s %-25s';

var fmt = _sprintf2.default.bind(null, fmtStr);

var existsSync = _pathExists2.default.sync;

var mkdirSync = _mkdirp2.default.sync;

var detached = new _web2.default();
var toWei = detached.toWei;
var fromWei = detached.fromWei;
var expr = _mathjs2.default.eval; // jshint ignore:line

var namespaceProps = ['contract', 'config', 'state'];

var keys = Object.keys;
var assign = Object.assign;
var create = Object.create;
var yellow = _chalk2.default.yellow;
var green = _chalk2.default.green;
var red = _chalk2.default.red;
var magenta = _chalk2.default.magenta;


function parseNamespace(data) {
  if (!data.contract) return null;
  return namespaceProps.reduce(function (r, v) {
    r[v] = data[v];
    return r;
  }, {});
}

EtherCommando.log = _log2.default;

function statePath() {
  /* jshint validthis:true */
  return (0, _path.join)(homePath.call(this), 'state.json');
}

function homePath() {
  /* jshint validthis:true */
  return (0, _path.join)((0, _os.homedir)(), '.' + this._title);
}

var ConfigProto = {
  save: function save() {
    /* jshint validthis:true */
    if (!existsSync(homePath.call(this._parent))) mkdirSync(homePath.call(this._parent));
    var parent = this._parent;
    delete this._parent;
    var retval = (0, _fs.writeFileSync)(statePath.call(parent), JSON.stringify(this, null, 1));
    this._parent = parent;
    return retval;
  }
};

function readOrCreateConfigObject() {
  /* jshint validthis:true */
  var link = { _parent: this };
  if (!existsSync(homePath.call(this))) mkdirSync(homePath.call(this));
  if (existsSync(statePath.call(this))) return assign(create(ConfigProto), assign(link, JSON.parse((0, _fs.readFileSync)(statePath.call(this), 'utf8'))));else return assign(create(ConfigProto), link);
}

function bootstrap(config) {
  var _this = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    var root = parseNamespace(config);
    _this._namespaces = assign({
      root: root ? root : {}
    }, (0, _lodash.difference)(keys(config), namespaceProps).reduce(function (r, v) {
      r[v] = parseNamespace(config[v]);
      return r;
    }, {}));
    _this._title = config.title;
    if (!_this._title) _this._title = (0, _path.parse)(process.argv[1]).name;
    process.title = _this._title;
    _this._state = readOrCreateConfigObject.call(_this);
    parseArgs.call(_this);
    readSavedAddresses.call(_this);
    bootstrapEthClient.call(_this).then(function () {
      return parseGasAndPrice.call(_this);
    }).then(function () {
      targetContract.call(_this);
      selectUser.call(_this);
      selectNamespace.call(_this);
      _this.action = buildAction.call(_this);
      resolve();
    }).catch(reject);
  });
}

function selectNamespace() {
  /* jshint validthis:true */
  if (!this._namespaces[this.opts.argv[0]]) this._selectedNamespace = 'root';else this._selectedNamespace = this.opts.argv[0];
}

function callTarget(namespace, address) {
  /* jshint validthis:true */
  (0, _validation.validateAddress)(address);
  this._state[namespace] = address;
  this._state.save();
  (0, _log.info)('using ' + green(address) + ' as ' + red(namespace));
}

function targetContract() {
  /* jshint validthis:true */
  var dispatch = this.opts.options.dispatch;

  if (dispatch) {
    var signedTx = JSON.parse((0, _fs.readFileSync)(dispatch, 'utf8'));
    return this.addr = signedTx.addr;
  }
  if (this._namespaces[this.opts.argv[0]]) this.addr = this._namespaces[this.opts.argv[0]].address;else this.addr = this._namespaces.root.address;
  if (this.opts.options.addr) this.addr = aliasToAddr.call(this, this.opts.options.addr);
}

function callAlias(name, addr) {
  /* jshint validthis:true */
  addr = aliasToAddr.call(this, addr);
  (0, _validation.validateAddress)(addr);
  if (!this._state.aliases) this._state.aliases = {};
  this._state.aliases[name] = addr;
  (0, _log.info)('using ' + green(addr) + ' as ' + red(name));
  this._state.save();
}

function selectUser() {
  /* jshint validthis:true */
  var dispatch = this.opts.options.dispatch;

  if (dispatch) {
    var signedTx = JSON.parse((0, _fs.readFileSync)(dispatch, 'utf8'));
    return this.from = signedTx.owner;
  }
  this.from = this.accounts[0];
  if (this.opts.options.from) this.from = aliasToAddr.call(this, this.opts.options.from);
}

function aliasToAddr(alias) {
  /* jshint validthis:true */
  if ((0, _util.isInteger)(alias)) {
    var accts = void 0;
    if (+alias >= 1e50) {
      alias = "0";
    }
    if ((alias = +alias) >= (accts = this.accounts).length) {
      throw _errors.UsageError.nonexistentAccount(alias);
    }
    return accts[alias];
  } else if (this._state[alias]) {
    return this._state[alias];
  } else if (this._state.aliases && this._state.aliases[alias]) {
    return this._state.aliases[alias];
  } else {
    return alias;
  }
}

function prompt(question) {
  return magenta('[prompt]') + ' ' + question;
}

function subsTilde(str) {
  return str.replace((0, _os.homedir)(), '~');
}

function bootstrapEthClient() {
  var _this2 = this;

  /* jshint validthis:true */
  try {
    var _ret = function () {
      var hostname = _this2.opts.options.host || _this2._state.rpc && _this2._state.rpc.hostname || 'localhost';
      var _opts$options = _this2.opts.options;
      var port = _opts$options.port;
      var hd = _opts$options.hd;
      var password = _opts$options.password;
      var unlock = _opts$options.unlock;
      var out = _opts$options.out;
      var local = _opts$options.local;
      var dispatch = _opts$options.dispatch;
      var protocol = _opts$options.protocol;
      var pathname = _opts$options.pathname;

      var passOverride = true;
      password = password || ((passOverride = false) || true) && _this2._state.rpc && _this2._state.rpc.password || null;
      var hdOverride = true;
      var localOverride = true;
      hd = hd || ((hdOverride = false) || true) && _this2._state.rpc && _this2._state.rpc.hd || null;
      unlock = unlock || _this2._state.rpc && _this2._state.rpc.unlock;
      local = local || ((localOverride = false) || true) && _this2._state.rpc && _this2._state.rpc.local || null;
      port = port || _this2._state.rpc && _this2._state.rpc.port || 8545;
      pathname = _this2._state.rpc && _this2._state.rpc.pathname;
      protocol = _this2._state.rpc && _this2._state.rpc.protocol || 'http:';
      if (passOverride) hd = true;
      if (!hdOverride && localOverride) {
        hd = false;
        local = true;
      }
      if (hdOverride && !localOverride) {
        hd = true;
        local = false;
      }
      var url = (0, _url.format)({
        hostname: hostname,
        port: port,
        pathname: pathname,
        protocol: protocol
      });
      if (port !== 8545 || hostname !== 'localhost' || protocol !== 'http:' || pathname) {
        if (!_this2._state.rpc) _this2._state.rpc = {};
        Object.assign(_this2._state.rpc, { port: port, hostname: hostname, protocol: protocol, pathname: pathname });
        _this2._state.save();
      }
      if (!_this2._state.rpc) _this2._state.rpc = {};
      Object.assign(_this2._state.rpc, {
        hd: hd,
        unlock: unlock,
        password: password,
        local: local
      });
      _this2._state.save();
      if (!hd && !dispatch) return {
          v: Promise.resolve((_this2.web3 = new _web2.default(new _web2.default.providers.HttpProvider(url))) && (_this2.accounts = _this2.web3.eth.accounts))
        };
      var seed = void 0;
      var passwd = void 0;
      var rawKey = void 0;
      var absolute = void 0;
      var fmtPassword = function fmtPassword() {
        if (absolute) return (0, _path.relative)(process.cwd(), password);
        return subsTilde(password);
      };
      if (password && !dispatch) {
        var parts = (0, _path.parse)(password);
        if (!parts.dir) {
          var keyPath = void 0;
          password = (0, _path.join)(keyPath = (0, _path.join)(homePath.call(_this2), 'keystores'), password);
          if (!existsSync(keyPath)) mkdirSync(keyPath);
        } else absolute = true;
      }
      if ((!password || out && password) && !dispatch) {
        seed = _readlineSync2.default.question(prompt('HD seed: '));
        passwd = _readlineSync2.default.question(prompt('keystore password: '));
        (0, _fs.writeFileSync)(password, JSON.stringify({ seed: seed, passwd: passwd }, null, 1));
        (0, _log.info)('keystore saved to ' + green(fmtPassword()));
      } else if (!dispatch) {
        if (!existsSync(password)) {
          passwd = _readlineSync2.default.question(prompt('new keystore password: '));
          (0, _fs.writeFileSync)(password, JSON.stringify((0, _ethereumjsWallet.generate)().toV3(passwd, { n: 2048 }), null, 1));
          (0, _log.info)('keystore saved to ' + green(fmtPassword()));
        } else {
          var saved = JSON.parse((0, _fs.readFileSync)(password, 'utf8'));
          if ((typeof saved === 'undefined' ? 'undefined' : _typeof(saved)) === 'object') {
            if (saved.version === 3) {
              saved.crypto = saved.Crypto || saved.crypto;
              rawKey = (0, _ethereumjsWallet.fromV3)(saved, (0, _readlineSync2.default)(prompt('keystore password: '))).getPrivateKey();
            } else {
              seed = saved.seed;
              passwd = saved.passwd;
              (0, _log.info)('using seed ' + magenta(seed));
              if (!passwd) passwd = (0, _readlineSync2.default)(prompt('keystore password: '));
            }
          } else {
            if (saved.substr(0, 2) === '0x') saved = saved.substr(2);
            rawKey = Buffer.from(saved, 'hex');
          }
        }
      }
      return {
        v: new Promise(function (resolve, reject) {
          if (dispatch) {
            var _ret2 = function () {
              var signedTx = JSON.parse((0, _fs.readFileSync)(dispatch, 'utf8'));
              return {
                v: resolve(_this2.web3 = new _web2.default(new _hookedWeb3Provider2.default({
                  host: url,
                  transaction_signer: {
                    hasAddress: function hasAddress(addr, cb) {
                      cb(null, true);
                    },
                    signTransaction: function signTransaction(tx, cb) {
                      cb(null, signedTx.tx);
                    }
                  }
                })))
              };
            }();

            if ((typeof _ret2 === 'undefined' ? 'undefined' : _typeof(_ret2)) === "object") return _ret2.v;
          }
          if (rawKey) {
            _this2.accounts = ['0x' + (0, _ethereumjsUtil.publicToAddress)((0, _ethereumjsUtil.privateToPublic)(rawKey)).toString('hex')];
            return resolve(_this2.web3 = new _web2.default(new _hookedWeb3Provider2.default({
              host: url,
              transaction_signer: {
                hasAddress: function hasAddress(addr, cb) {
                  return cb(null, addr === _this2.accounts[0]);
                },
                signTransaction: function signTransaction(tx, cb) {
                  var o = new _ethereumjsTx2.default(tx);
                  o.sign(rawKey);
                  return cb(null, '0x' + o.serialize().toString('hex'));
                }
              }
            })));
          }
          _ethLightwallet2.default.keystore.createVault({
            seedPhrase: seed,
            password: passwd
          }, function (err, transaction_signer) {
            _this2.keystore = transaction_signer;
            if (err) return reject(err);
            transaction_signer.keyFromPassword(passwd, function (err, result) {
              if (err) return reject(err);
              transaction_signer.passwordProvider = function (cb) {
                return cb(null, passwd);
              };
              _this2.pwDerivedKey = result;
              transaction_signer.generateNewAddress(result, unlock || 12);
              _this2.accounts = transaction_signer.getAddresses().map(function (v) {
                if (v.substr(0, 2) !== '0x') return '0x' + v;
                return v;
              });
              var sign = _this2.opts.options.sign;

              if (sign) _this2.web3 = new _web2.default(new _hookedWeb3Provider2.default({
                host: url,
                transaction_signer: {
                  hasAddress: function hasAddress(addr, cb) {
                    return transaction_signer.hasAddress(addr, cb);
                  },
                  signTransaction: function signTransaction(txParams, cb) {
                    var retval = transaction_signer.signTransaction(txParams, function (err, signed) {
                      (0, _fs.writeFileSync)(sign, JSON.stringify({
                        tx: signed,
                        date: new Date(),
                        owner: _this2.from,
                        namespace: _this2.namespaceName,
                        call: _this2.func,
                        args: _this2.args,
                        addr: _this2.addr
                      }, null, 1));
                      (0, _log.info)('transaction saved to ' + green(sign));
                      process.exit(0);
                    });
                    return retval;
                  }
                }
              }));else _this2.web3 = new _web2.default(new _hookedWeb3Provider2.default({
                host: url,
                transaction_signer: transaction_signer
              }));
              resolve(_this2.web3);
            });
          });
        })
      };
    }();

    if ((typeof _ret === 'undefined' ? 'undefined' : _typeof(_ret)) === "object") return _ret.v;
  } catch (e) {
    return Promise.reject(e);
  }
}

function readSavedAddresses() {
  var _this3 = this;

  /* jshint validthis:true */
  keys(this._state).forEach(function (v) {
    if (v === 'rpc' || v === 'aliases') return;
    var realKey = v;
    if (v === _this3._title) realKey = 'root';
    if (!_this3._namespaces[realKey]) _this3._namespaces[realKey] = {};
    _this3._namespaces[realKey].address = _this3._state[v];
    if (!_this3._namespaces[realKey].config) _this3._namespaces[realKey].config = {};
    if (_this3.opts.addr) _this3._namespaces[realKey].address = _this3.opts.addr;
  });
  this._aliases = this._state.aliases || {};
}

var cmds = [['help', 'display available namespaces and root contract'], ['[namespace] help', 'display functions in [namespace]'], ['sendEther [account] [amount]', 'send [amount] ETH to [account]'], ['info', 'output root namespace address and account used'], ['[namespace] info', 'output address for [namespace]'], ['target [address]', 'make [address] default address for root contract'], ['[namespace] target [address]', 'make [address] default address for [namespace]'], ['[namespace] siphon [filename]', 'download contract state into [filename]'], ['[namespace] upload [filename]', 'upload state from [filename] to [namespace]'], ['etherBalanceOf [address]', 'check ETH balance at [address]'], ['deploy [-t]', 'deploy root contract'], ['[namespace] deploy [-t]', 'deploy [namespace] contract'], ['alias [name] [address]', 'set alias']];

function parseArgs() {
  var _this4 = this;

  /* jshint validthis:true */
  var getopt = new _nodeGetopt2.default([['h', 'help', 'display this help'], ['q', 'no-event', 'do not wait for transaction to be mined'], ['r', 'price=GASPRICE', 'gasPrice value (saves)'], ['v', 'version', 'display version number'], ['p', 'port=PORT', 'JSON-RPC port number (saves)'], ['g', 'gas=GAS', 'maximum quantity of gas (saves)'], ['a', 'addr=ADDRESS', 'contract address'], ['j', 'host=HOST', 'JSON-RPC hostname (saves)'], ['t', 'target', 'target new contract automatically after deploy'], ['f', 'from=ADDRESS', 'Ethereum address to send transactions from'], ['d', 'hd', 'enable HD wallet (saves)'], ['l', 'local', 'use local accounts on RPC (saves)'], ['n', 'unlock=NUMACCOUNTS', 'number of HD wallets to unlock (saves)'], ['x', 'password=PASSWORDFILE', 'supply path to HD seed/password (saves)'], ['o', 'out', 'force prompt for HD credentials to save'], ['s', 'sign=FILE', 'sign transaction and save to file'], ['u', 'dispatch=FILE', 'dispatch a saved transaction']]);
  getopt.setHelp(yellow('[help]') + ' ' + grayBold(this._title + ' [-hvtdo] [-p PORT] [-g GAS] [-a ADDR] [-f FROM] [-n NUM] [-x FILE] [NAMESPACE] COMMAND') + _os.EOL + yellow('commands:') + _os.EOL + cmds.map(function (v) {
    return grayBold(fmt.apply(null, v));
  }).join(_os.EOL) + _os.EOL + yellow('options:') + _os.EOL + grayBold('[[OPTIONS]]'));
  getopt.errorFunc = function (e) {
    _this4._error = e;
  };
  this.opts = getopt.parseSystem();
  if (this.opts.options.help) {
    getopt.showHelp();
    process.exit(0);
  }
}

function reportGasPrices(gas, gasPrice) {
  if (!gas) gas = 'default';else gas = (+gas).toExponential();
  if (!gasPrice) gasPrice = 'default';else gasPrice = (+gasPrice).toExponential() + ' wei';
  (0, _log.info)('posting ' + yellow(gas) + ' gas with gasPrice of ' + yellow(gasPrice));
}

var doesntRequireTarget = ['deploy', 'target', 'sendEther', 'etherBalanceOf', 'alias'];

function buildAction() {
  var _this5 = this;

  /* jshint validthis:true */
  var namespace = this._namespaces.root,
      namespaceName = 'root',
      func = void 0,
      args = void 0,
      funcObj = void 0,
      contract = void 0,
      Contract = void 0;
  var dispatch = this.opts.options.dispatch;

  return function () {
    if (_this5._error) return Promise.reject(_this5._error);
    try {
      if (dispatch) {
        var signedTx = JSON.parse((0, _fs.readFileSync)(dispatch, 'utf8'));
        namespaceName = signedTx.namespace;
        namespace = _this5._namespaces[namespaceName];
        func = _this5.func = signedTx.call;
        args = _this5.args = signedTx.args;
      } else {
        if (_this5._namespaces[_this5.opts.argv[0]]) {
          namespace = _this5._namespaces[_this5.opts.argv[0]];
          namespaceName = _this5.namespaceName = _this5.opts.argv[0];
          func = _this5.func = _this5.opts.argv[1];
          args = _this5.args = _this5.opts.argv.slice(2);
        } else {
          namespaceName = _this5.namespaceName = _this5._title || namespace.name;
          func = _this5.func = _this5.opts.argv[0];
          args = _this5.args = _this5.opts.argv.slice(1);
        }
      }
      if (!func) return callInfo.call(_this5, _this5.from, _this5.addr, namespaceName);
      switch (func) {
        case 'target':
          return Promise.resolve(callTarget.call(_this5, namespaceName, args[0]));
        case 'alias':
          return Promise.resolve(callAlias.call(_this5, args[0], args[1]));
        case 'sendEther':
          return sendEther.call(_this5);
        case 'etherBalanceOf':
          return etherBalanceOf.call(_this5);
        case 'getGasPrice':
          return getGasPrice.call(_this5, args[0]);
        case 'getTransactionReceipt':
          return getTransactionReceipt.call(_this5, args[0]);
        case 'sign':
          return sign.apply(_this5, args);
        case 'getGasLimit':
          return getGasLimit.call(_this5, args[0]);
        case 'getBlock':
          return getBlock.call(_this5, args[0], args[1]);
        case 'getBlockNumber':
          return getBlockNumber.call(_this5);
        case 'getSyncing':
          return getSyncing.call(_this5);
      }
      Contract = _this5.Contract = namespace.contract && _this5.web3.eth.contract(namespace.contract.abi) || {};
      switch (func) {
        case 'help':
          return help.call(_this5, namespaceName, namespace.contract && namespace.contract.abi);
        case 'deploy':
          return deploy.call(_this5, namespaceName, namespace, args);
        case 'info':
          return callInfo.call(_this5, _this5.from, _this5.addr, namespaceName);
      }
      contract = _this5.contract = Contract.at(_this5.addr);
      switch (func) {
        case 'siphon':
          return siphon.call(_this5, namespaceName, namespace, args[0]);
        case 'upload':
          return upload.call(_this5, namespace, args[0]);
      }
      if (!(funcObj = namespace.contract.abi.find(function (v) {
        return v.name === func;
      }))) return Promise.reject((0, _errors.UsageError)('function/namespace ' + func + ' not found'));

      reportAccount(_this5.from);
      if (!~doesntRequireTarget.indexOf(func) && _this5.addr) (0, _log.info)('using ' + green(_this5.addr) + ' for ' + red(namespaceName));else if (!_this5.addr) return Promise.reject((0, _errors.UsageError)('must supply address for ' + namespaceName));
    } catch (e) {
      return Promise.reject(e);
    }
    return new Promise(function (resolve, reject) {
      args.forEach(function (v, i, arr) {
        if (funcObj.inputs[i]) {
          switch (funcObj.inputs[i].type) {
            case 'address':
              arr[i] = aliasToAddr.call(_this5, v);
              break;
          }
        }
      });
      var promise = namespace.config && namespace.config[func] && namespace.config[func].transformArgs && namespace.config[func].transformArgs.call(_this5, args) || Promise.resolve(args);
      promise.then(function (args) {
        args = convertArgs.call(_this5, args, funcObj);
        var argStr = argString.call(_this5, args, funcObj);
        if (funcObj.constant || namespace.config && namespace.config[func] && namespace.config[func].constant) {
          (0, _log.info)('calling ' + green(func) + ' on ' + red(namespaceName) + (args.length ? ' with argument' + (args.length > 1 ? 's' : '') + ' ' + argStr : ''));
          var from = _this5.from;

          args.push({ from: from });
          args.push(function () {
            var afterFirst = false;
            return function (err, result) {
              var thisRun = afterFirst;
              afterFirst = true;
              if (!thisRun) {
                if (err) return reject(err);
                if (namespace.config && namespace.config[func] && namespace.config[func].hook) {
                  namespace.config[func].hook.call(_this5, result, resolve, reject);
                } else {
                  (0, _log.ln)(String(result));
                  resolve(result);
                }
              }
            };
          }());
          contract[func].call.apply(contract[func], args);
        } else {
          (0, _log.info)('transacting ' + green(func) + ' with ' + red(namespaceName) + (args.length ? ' using argument' + (args.length > 1 ? 's' : '') + ' ' + argStr : ''));
          var _from = _this5.from;
          var gas = _this5.gas;
          var gasPrice = _this5.gasPrice;

          var txCfg = { from: _from, gas: gas, gasPrice: gasPrice };
          if (!gas) delete txCfg.gas;
          if (!gasPrice) delete txCfg.gasPrice;
          reportGasPrices(gas, gasPrice);
          args.push(txCfg);
          var noEvent = _this5.opts.options['no-event'];

          if (namespace.config && namespace.config[func] && namespace.config[func].hook && !noEvent) {
            args.push(function (err, result) {
              if (err) return reject(err);
              reportTx.call(_this5, result);
              namespace.config[func].hook.call(_this5, result, resolve, reject);
            });
          } else if (noEvent || namespace.config && namespace.config[func] && namespace.config[func].noEvent) {
            args.push(function (err, result) {
              if (err) return reject(err);
              reportTx.call(_this5, result);
              pollForMinedReceipt.call(_this5, result, resolve);
            });
          } else {
            args.push(function (err, tx) {
              if (err) return reject(err);
              reportTx.call(_this5, tx);
              var filter = _this5.web3.eth.filter({
                address: _this5.addr,
                topics: []
              });
              (0, _log.info)('waiting for transaction to be mined...');
              filter.watch(function (err, result) {
                if (err) {
                  filter.stopWatching();
                  return reject(err);
                }
                if (result.transactionHash === tx) {
                  filter.stopWatching();
                  (0, _log.info)('transaction mined!');
                  return _this5.web3.eth.getTransactionReceipt(tx, function (err, result) {
                    if (err) return reject(err);
                    (0, _log.info)('gas used: ' + yellow(result.cumulativeGasUsed));
                    resolve(tx);
                  });
                }
              });
            });
          }
        }
        contract[func].apply(contract[func], args);
      }).catch(function (err) {
        reject(err);
      });
    });
  };
}
function logHelp(msg) {
  return (0, _log.ln)(yellow('[help]') + ' ' + msg);
}

function reportAccount(acct) {
  if (!acct) return (0, _log.info)('no account selected');
  return (0, _log.info)('using account ' + green(acct));
}

function sign() {
  var _this6 = this;

  /* jshint validthis:true */
  var from = this.from;

  var args = [from].concat([].slice.call(arguments));
  if (this.keystore) return new Promise(function (resolve, reject) {
    args.splice(0, 1);
    var sig = _ethLightwallet2.default.signing.concatSig(_ethLightwallet2.default.signing.signMsgHash(_this6.keystore, _this6.pwDerivedKey, _this6.web3.sha3.apply(_this6.web3, args), _this6.from));
    return resolve(sig);
  });
  return new Promise(function (resolve, reject) {
    args.push(function (err, result) {
      if (err) return reject(err);
      (0, _log.info)(result);
      resolve(result);
    });
    console.log(require('util').inspect(args));
    _this6.web3.eth.sign.apply(_this6.web3.eth, args);
  });
}

function reportRPC() {
  /* jshint validthis:true */
  var rpcURI = getRPCURI.call(this);
  if (!rpcURI) (0, _log.info)('no RPC selected');
  (0, _log.info)('using RPC at ' + green(rpcURI));
}

function callInfo(from, addr, namespaceName) {
  /* jshint validthis:true */
  reportRPC.call(this);
  reportAccount(from);
  if (addr) (0, _log.info)('using ' + green(addr) + ' for ' + red(namespaceName));
  return Promise.resolve();
}

function grayBold(d) {
  return _chalk2.default.gray(_chalk2.default.bold(d));
}

function help(namespaceName, abi) {
  /* jshint validthis:true */
  if (namespaceName === this._title) {
    logHelp('available help categories:');
    (0, _log.ln)(Object.keys(this._namespaces).filter(function (v) {
      switch (v) {
        case 'root':
        case '_parent':
          return false;
        default:
          return true;
      }
    }).map(function (v) {
      return '  ' + grayBold(v);
    }).join(_os.EOL));
  }
  if (abi) {
    logHelp('available functions in namespace ' + red(namespaceName));
    abi = abi.slice().filter(function (v) {
      return v.type === 'function';
    });
    abi.sort(function (a, b) {
      return a.name > b.name;
    });
    (0, _log.ln)(abi.map(function (v) {
      return '  ' + grayBold(v.name) + (v.inputs && v.inputs.length && ' ' || '') + (v.inputs && v.inputs.map(function (v) {
        return grayBold('[' + v.type + ']');
      }).join(' ') || '');
    }).join(_os.EOL));
  }
}

function deleteNullKeys(obj) {
  keys(obj).forEach(function (v) {
    if (_typeof(obj[v]) === 'object') deleteNullKeysHelper(obj[v]);
  });
}

function deleteNullKeysHelper(obj) {
  if (!Array.isArray(obj)) {
    (0, _lodash.forOwn)(obj, function (value, key, obj) {
      if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object') {
        deleteNullKeys(value);
        if (!keys(value).length) delete obj[key];
      } else if (value === 0 || value === false) delete obj[key];
    });
  } else {
    obj.forEach(function (v) {
      if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) === 'object') deleteNullKeys(v);
    });
  }
}

function siphon(namespaceName, namespace, filename) {
  var _this7 = this;

  return new Promise(function (resolve, reject) {
    if (!filename) filename = namespaceName + '-' + String(Date.now()) + '.json';
    if (!namespace.state) throw (0, _errors.UsageError)('no state defined for contract');
    var state = {},
        cfg = namespace.state,
        ref = cfg,
        ks = void 0;
    (0, _async.mapSeries)(ks = keys(cfg), function (v, next) {
      (0, _log.info)('siphoning ' + v);
      ref = cfg[v];
      var get = ref.get = ref.get || v;
      var path = [];
      while (ref.indexedBy) {
        path.push({
          get: ref.indexedBy.get,
          length: ref.indexedBy.length,
          ref: ref
        });
        ref = ref.indexedBy;
      }
      if (!path.length) {
        return _this7.contract[ref.get].call(function (err, result) {
          if (err) return next(err);
          if (typeof result.lessThan === 'function') result = +result;
          next(null, result);
        });
      }
      path.reverse();
      var self = _this7;
      (function siphonMapping(seg, path, i, keys, cb) {
        if (!keys) keys = [];
        keys.push(function (err, result) {
          if (err) return cb(err);
          if (typeof result.lessThan === 'function') result = +result;
          keys.pop();
          (0, _async.mapSeries)((0, _lodash.range)(result), function (v, next) {
            keys.push(v);
            keys.push(function (err, result) {
              if (typeof result.lessThan === 'function') result = +result;
              keys.pop();
              keys.pop();
              next(err, result);
            });
            self.contract[seg.get].call.apply(self.contract[seg.get], keys);
          }, function (err, result) {
            if (err) return cb(err);
            if (path.length !== i + 1) {
              (0, _async.mapSeries)(result, function (v, next) {
                keys.push(v);
                siphonMapping(path[i + 1], path, i + 1, keys, function (err, results) {
                  keys.pop();
                  next(err, results.reduce(function (r, v, i) {
                    r.push([result[i]].concat(v));
                    return r;
                  }, []));
                });
              }, function (err, results) {
                if (err) return cb(err);
                return cb(null, results.reduce(function (r, v) {
                  return r.concat(v);
                }, []));
              });
            } else cb(null, result.map(function (v) {
              return [v];
            }));
          });
        });
        self.contract[seg.length].call.apply(self.contract[seg.length], keys);
      })(path[0], path, 0, [], next);
    }, function (err, results) {
      if (err) return reject(err);
      var i = -1;
      (0, _async.mapSeries)(results, function (v, next) {
        i++;
        if (!Array.isArray(v)) return next(null, v);
        (0, _async.mapSeries)(v, function (w, next) {
          w.push(function (err, result) {
            if (typeof result.lessThan === 'function') result = +result;
            w.pop();
            next(err, result);
          });
          _this7.contract[cfg[ks[i]].get].call.apply(_this7.contract[cfg[ks[i]].get], w);
        }, next);
      }, function (err, result) {
        result = (0, _lodash.zipObject)(ks, result.map(function (v, i) {
          if (!Array.isArray(v)) return v;
          return v.reduce(function (r, v, idx) {
            var ref = r,
                last = void 0;
            results[i][idx].forEach(function (v, i, arr) {
              if (i === arr.length - 1) return last = v;
              if (!ref[v]) {
                ref[v] = {};
                ref = ref[v];
              }
            });
            ref[last] = v;
            return r;
          }, {});
        }));
        deleteNullKeys(result);
        (0, _fs.writeFileSync)(filename, JSON.stringify(result, null, 1));
        (0, _log.info)(red(namespaceName) + ' saved to ' + green(filename));
        resolve(result);
      });
    });
  });
}

function objectTo3DArray(obj) {
  return keys(obj).map(function (v) {
    return [v, objectTo3DArrayHelper(obj[v])];
  });
}

function objectTo3DArrayHelper(obj) {
  if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object') return keys(obj).reduce(function (r, v) {
    return r.concat(objectTo3DArrayHelper(obj[v]).map(function (i) {
      i.unshift(v);
      return i;
    }));
  }, []);
  return [[obj]];
}

function upload(namespace, filename) {
  var _this8 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    var from = _this8.from;
    var gas = _this8.gas;
    var gasPrice = _this8.gasPrice;

    var txCfg = { from: from, gas: gas, gasPrice: gasPrice };
    if (!gas) delete txCfg.gas;
    if (!gasPrice) delete txCfg.gasPrice;
    var cfg = namespace.state,
        ref = void 0;
    var obj = JSON.parse((0, _fs.readFileSync)(filename, 'utf8'));
    deleteNullKeys(obj);
    if (!cfg) throw (0, _errors.UsageError)('no state defined for contract');
    (0, _async.mapSeries)((0, _log.ln)(objectTo3DArray(obj)), function (v, next) {
      (0, _log.info)('uploading ' + v[0]);
      (0, _async.mapSeries)(v[1], function (w, next) {
        if (!Array.isArray(w)) w = [w];
        w.push(txCfg);
        w.push(next);
        _this8.contract[cfg[v[0]].set].sendTransaction.apply(_this8.contract[cfg[v[0]].set], w);
      }, next);
    }, function (err, result) {
      if (err) return reject(err);
      (0, _log.info)('done!');
      resolve();
    });
  });
}

function sendEther() {
  var _this9 = this;

  /* jshint validthis:true */
  this.opts.argv[1] = aliasToAddr.call(this, this.opts.argv[1]);
  (0, _validation.validateAddress)(this.opts.argv[1]);
  (0, _validation.validateDouble)(this.opts.argv[2]);
  (0, _log.info)('sending ' + red(this.opts.argv[2]) + ' ether to ' + green(this.opts.argv[1]));
  var from = this.from;
  var gas = this.gas;
  var gasPrice = this.gasPrice;

  var _opts$argv = _slicedToArray(this.opts.argv, 3);

  var _ = _opts$argv[0];
  var to = _opts$argv[1];
  var value = _opts$argv[2];

  value = toWei(+value, "ether");
  var txCfg = { from: from, gas: gas, gasPrice: gasPrice, to: to, value: value };
  reportGasPrices(gas, gasPrice);
  if (!gas) delete txCfg.gas;
  if (!gasPrice) delete txCfg.gasPrice;
  return new Promise(function (resolve, reject) {
    _this9.web3.eth.sendTransaction(txCfg, function (err, tx) {
      if (err) return reject(err);
      (0, _log.info)('created transaction at ' + green(tx));
      pollForMinedReceipt.call(_this9, tx, resolve);
    });
  });
}

function defer() {
  var resolve = void 0,
      reject = void 0,
      promise = new Promise(function (_resolve, _reject) {
    resolve = _resolve;
    reject = _reject;
  });
  return {
    promise: promise,
    resolve: resolve,
    reject: reject
  };
}

function timeout(n) {
  var timer = null,
      deferred = defer();
  var cancel = function cancel() {
    if (timer) {
      deferred.reject(Error('@@interrupt'));
      clearTimeout(timer);
    }
  };
  timer = setTimeout(function () {
    deferred.resolve();
    timer = null;
  }, n);
  return {
    cancel: cancel,
    promise: deferred.promise
  };
}

var POLL_INTERVAL = 5000;

function dispatchPoll(tx, resolve, cancel) {
  var _this10 = this;

  /* jshint validthis:true */
  var _timeout = timeout(POLL_INTERVAL);

  var promise = _timeout.promise;

  return promise.then(function () {
    return pollOne.call(_this10, tx, resolve, cancel);
  });
}

function pollOne(tx, resolve, cancel) {
  var _this11 = this;

  /* jshint validthis:true */
  var receipt = void 0;
  return getTransactionReceiptQuiet.call(this, tx).then(function (_receipt) {
    receipt = _receipt;
    return getBlockNumberQuiet.call(_this11);
  }).then(function (number) {
    if (receipt && (receipt.logs && receipt.logs.find(function (v) {
      return v.type === 'mined';
    }) || receipt.blockNumber >= number)) {
      cancel();
      (0, _log.info)('transaction mined!');
      return resolve(receipt);
    }
    return dispatchPoll.call(_this11, tx, resolve, cancel);
  });
}

function pollForMinedReceipt(tx, resolve) {
  /* jshint validthis:true */
  var _timeout2 = timeout(3000);

  var cancel = _timeout2.cancel;
  var promise = _timeout2.promise;

  promise.then(function () {
    return (0, _log.info)('waiting for transaction to be mined...');
  }).catch(function (err) {
    return err;
  });
  return pollOne.call(this, tx, resolve, cancel);
}

function deploy(namespaceName, namespace, args) {
  var _this12 = this;

  /* jshint validthis:true */
  var contract = namespace.contract;
  var cons = contract.abi.find(function (v) {
    return v.type === 'constructor';
  });
  var defArgs = (cons && cons.inputs || []).map(function (v) {
    if (/uint/.test(v.type)) return 0;
    if (/string/.test(v.type)) return "";
    if (v.type === 'bool') return false;
    return "0x0";
  });
  args.forEach(function (v, i) {
    if ((cons.inputs[i] || {}).type === 'address') defArgs[i] = aliasToAddr.call(_this12, v);else if (/uint/.test((cons.inputs[i] || {}).type)) defArgs[i] = +v;else defArgs[i] = v;
  });
  args = defArgs;
  var from = this.from;
  var gas = this.gas;
  var gasPrice = this.gasPrice;

  var data = contract.binary;
  var txCfg = { from: from, gas: gas, gasPrice: gasPrice, data: data };
  reportGasPrices(gas, gasPrice);
  if (!gas) delete txCfg.gas;
  if (!gasPrice) delete txCfg.gasPrice;
  args.push(txCfg);
  return new Promise(function (resolve, reject) {
    var timer = void 0;
    args.push(function (err, result) {
      if (err) return reject(err);
      if (!result.address) {
        pollForMinedReceipt.call(_this12, result.transactionHash, resolve);
      } else {
        (function () {
          if (timer) clearTimeout(timer);
          (0, _log.info)(red(namespaceName) + ' deployed to ' + green(result.address));
          var address = result.address;

          if (result.transactionHash) _this12.web3.eth.getTransactionReceipt(result.transactionHash, function (err, result) {
            (0, _log.info)('gas used: ' + yellow(result.cumulativeGasUsed));
            resolve(address);
          });
        })();
      }
    });
    _this12.Contract.new.apply(_this12.Contract, args);
  }).then(function (receipt) {
    return Promise.resolve(callTarget.call(_this12, namespaceName, receipt.contract));
  });
}

function etherBalanceOf() {
  var _this13 = this;

  /* jshint validthis:true */
  this.opts.argv[1] = aliasToAddr.call(this, this.opts.argv[1]);
  (0, _validation.validateAddress)(this.opts.argv[1]);
  return new Promise(function (resolve, reject) {
    var balance = _this13.web3.eth.getBalance(_this13.opts.argv[1]);
    (0, _log.info)('current balance: ' + green(String(fromWei(balance, "ether")) + ' ETH'));
    resolve(balance);
  });
}

function reportTx(result) {
  /* jshint validthis:true */
  (0, _log.info)('transaction available at ' + green(result));
  var state = this._state;

  state.data = state.data || {};
  state.data.lastTx = result;
  state.save();
}

function keyToRegExp(key) {
  return RegExp((0, _escapeRegexp2.default)(key), 'ig');
}

function exprSub(exp, vals) {
  var ks = Object.keys(vals);
  ks.sort(function (a, b) {
    return a.length < b.length;
  });
  ks.forEach(function (k) {
    exp = exp.replace(keyToRegExp(k), String(vals[k]));
  });
  return expr(exp);
}

var unitMap = {
  'wei': 1,
  'kwei': 1000,
  'ada': 1000,
  'femtoether': 1000,
  'mwei': 1000000,
  'babbage': 1000000,
  'picoether': 1000000,
  'gwei': 1000000000,
  'shannon': 1000000000,
  'nanoether': 1000000000,
  'nano': 1000000000,
  'szabo': 1000000000000,
  'microether': 1000000000000,
  'micro': 1000000000000,
  'finney': 1000000000000000,
  'milliether': 1000000000000000,
  'milli': 1000000000000000,
  'ether': 1000000000000000000,
  'eth': 1000000000000000000,
  'kether': 1000000000000000000000,
  'grand': 1000000000000000000000,
  'einstein': 1000000000000000000000,
  'mether': 1000000000000000000000000,
  'gether': 1000000000000000000000000000,
  'tether': 1000000000000000000000000000000
};

function mapOwn(obj, cb, thisArg) {
  var retval = Object.create(Object.getPrototypeOf(obj));
  (0, _lodash.forOwn)(obj, function (value, key) {
    retval[key] = cb.call(thisArg, value, key, obj);
  });
  return retval;
}

function exprSubCurrencies(exp, vals) {
  Object.assign(vals, mapOwn(unitMap, function (value) {
    return '*' + String(value);
  }));
  return exprSub(exp, vals);
}

var txHashRe = /0x[a-f0-9]{32}/i;

function getTransactionReceiptQuiet(hash) {
  var _this14 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    var data = _this14._state.data;

    if (hash === 'last') hash = data && data.lastTx;
    if (!txHashRe.test(hash)) throw Error('invalid transaction hash: ' + hash);
    _this14.web3.eth.getTransactionReceipt(hash, function (err, result) {
      if (err) return reject(err);
      resolve(result);
    });
  });
}

function getTransactionReceipt(hash) {
  var _this15 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    var data = _this15._state.data;

    if (hash === 'last') hash = data && data.lastTx;
    if (!txHashRe.test(hash)) throw Error('invalid transaction hash: ' + hash);
    (0, _log.info)('getting transaction receipt for ' + hash);
    _this15.web3.eth.getTransactionReceipt(hash, function (err, result) {
      if (err) return reject(err);
      (0, _log.ln)(colorInspect(result));
    });
  });
}

var colorsTrue = { colors: true };

function colorInspect(data) {
  return (0, _util2.inspect)(data, colorsTrue);
}

var trueRe = /_/.test.bind(/^true$/i);

function stringToBool(val) {
  if (trueRe(String(val))) return true;
  return false;
}

function getBlockNumber() {
  var _this16 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    _this16.web3.eth.getBlockNumber(function (err, result) {
      if (err) return reject(err);
      (0, _log.info)('current block number: ' + colorInspect(+result));
    });
  });
}

function getBlockNumberQuiet() {
  var _this17 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    _this17.web3.eth.getBlockNumber(function (err, result) {
      if (err) return reject(err);
      resolve(result);
    });
  });
}

function getBlock(arg, transactionObjects) {
  var _this18 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    arg = !isNaN(arg) && +arg || arg;
    transactionObjects = stringToBool(transactionObjects);
    (0, _log.info)('getting block ' + colorInspect(arg));
    _this18.web3.eth.getBlock(arg, transactionObjects, function (err, result) {
      if (err) return reject(err);
      (0, _log.ln)(colorInspect(result));
    });
  });
}

function getSyncing() {
  var _this19 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    (0, _log.info)('testing if Ethereum node is syncing');
    _this19.web3.eth.getSyncing(function (err, result) {
      if (err) return reject(err);
      (0, _log.ln)(colorInspect(result));
    });
  });
}

function getRPCURI() {
  /* jshint validthis:true */
  var rpc = this._state.rpc;

  if (!rpc || !rpc.hostname) return '';
  var hostname = rpc.hostname;
  var port = rpc.port;

  return (0, _url.format)({
    protocol: 'http:',
    hostname: hostname,
    port: port
  });
}

var mathLog = Math.log;
var floor = Math.floor;
var pow = Math.pow;


var pow10 = pow.bind(null, 10);

function zeroes(v) {
  v = +v;
  return floor(mathLog(v) / mathLog(10));
}

function numToDenom(v, precision) {
  v = +v;
  var numZeroes = zeroes(v);
  var multiple = floor(numZeroes / 3) * 3;
  var unit = keys(unitMap).find(function (v) {
    return zeroes(unitMap[v]) === multiple;
  }) || 'wei';
  var amt = v / pow10(multiple);
  if (precision === void 0) precision = 3;
  amt = floor(amt * pow10(precision)) / pow10(precision);
  return amt + ' ' + unit;
}

function getGasPrice() {
  var _this20 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    _this20.web3.eth.getGasPrice(function (err, result) {
      if (err) return reject(err);
      result = +result;
      (0, _log.info)('gas price: ' + green(numToDenom(result)));
      resolve(result);
    });
  });
}

function getGasLimit() {
  var _this21 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    _this21.web3.eth.getBlock('latest', function (err, block) {
      if (err) return reject(err);
      (0, _log.info)('gas limit: ' + green(block.gasLimit));
      resolve(block.gasLimit);
    });
  });
}

function parseGasAndPrice() {
  var _this22 = this;

  /* jshint validthis:true */
  return new Promise(function (resolve, reject) {
    var _opts$options2 = _this22.opts.options;
    var gasPrice = _opts$options2.price;
    var gas = _opts$options2.gas;
    var _opts$options3 = _this22.opts.options;
    var originalGasPrice = _opts$options3.price;
    var originalGas = _opts$options3.gas;

    if (!_this22._state.rpc) _this22._state.rpc = {};
    var _state$rpc = _this22._state.rpc;
    var savedGasPrice = _state$rpc.gasPrice;
    var savedGas = _state$rpc.gas;

    Promise.all([new Promise(function (res, rej) {
      _this22.web3.eth.getBlock('latest', function (err, block) {
        if (err) return rej(err);
        res(block);
      });
    }), new Promise(function (res, rej) {
      _this22.web3.eth.getGasPrice(function (err, result) {
        if (err) return rej(err);
        res(+result);
      });
    })]).then(function (results) {
      var _results = _slicedToArray(results, 2);

      var block = _results[0];
      var currentGasPrice = _results[1];

      if (!gasPrice && savedGasPrice) {
        gasPrice = savedGasPrice;
        originalGasPrice = savedGasPrice;
      }
      if (!gas && savedGas) {
        gas = savedGas;
        originalGas = savedGas;
      }
      if (gasPrice && gasPrice !== 'default' && isNaN(gasPrice)) gasPrice = exprSubCurrencies(gasPrice, { current: currentGasPrice });
      if (gas && gas !== 'default' && isNaN(gas)) gas = exprSub(gas, { max: block.gasLimit });
      if (gas === 'default') gas = void 0;else gas = +gas;
      if (gasPrice === 'default') gasPrice = void 0;else gasPrice = +gasPrice;
      var gasProps = { gas: originalGas, gasPrice: originalGasPrice };
      Object.assign(_this22, { gas: gas, gasPrice: gasPrice });
      if (!_this22._state.rpc) _this22._state.rpc = {};
      Object.assign(_this22._state.rpc, gasProps);
      _this22._state.save();
      resolve();
    }).catch(function (err) {
      reject(err);
    });
  });
}

function convertArgs(args, abiDesc) {
  var _this23 = this;

  /* jshint validthis:true */
  if (args.length !== abiDesc.inputs.length) throw (0, _errors.UsageError)('function expects ' + abiDesc.inputs.length + ' arguments, ' + args.length + ' provided');
  return args.map(function (v, i) {
    if (abiDesc.inputs[i].type === 'address') return aliasToAddr.call(_this23, v);
    if (/uint/.test(abiDesc.inputs[i].type)) return +v;
    if (abiDesc.inputs[i].type === 'bool') {
      if (v === 'true') return true;
      return false;
    }
    return v;
  });
}

function argString(args, abiDesc) {
  var compare = void 0;
  return args.map(function (v, i) {
    switch (compare = abiDesc.inputs[i].type) {
      case 'bool':
        return _chalk2.default.yellow(String(v));
      case 'address':
        return _chalk2.default.green(v);
      default:
        if (/uint/.test(v)) return _chalk2.default.red(v);
        return String(v);
    }
  }).join(', ');
}

function EtherCommando() {
  var _this24 = this;

  var config = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

  if (!(this instanceof EtherCommando)) return new EtherCommando(config);
  return bootstrap.call(this, config).then(function () {
    return _this24.action();
  });
}

EtherCommando.prototype = {
  callTarget: callTarget,
  pollForMinedReceipt: pollForMinedReceipt
};

module.exports = EtherCommando;
//# sourceMappingURL=ether-commando.js.map
