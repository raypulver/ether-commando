"use strict";

var _chalk = require('chalk');

var _chalk2 = _interopRequireDefault(_chalk);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _console = console;
var log = _console.log;
var cerror = _console.error;


module.exports = {
  info: function info(v) {
    log(_chalk2.default.cyan('[info]') + ' ' + v);
    return v;
  },
  warning: function warning(v) {
    log(_chalk2.default.yellow('[warning]') + ' ' + v);
    return v;
  },
  ln: function ln(v) {
    log(v);
    return v;
  },
  error: function error(v) {
    cerror(_chalk2.default.red('[error]') + ' ' + v);
    return v;
  }
};
//# sourceMappingURL=log.js.map
