"use strict";

var _errors = require('./errors');

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

var _util = require('./util');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var detached = new _web2.default();

var isAddress = detached.isAddress;


function validateAddress(addr) {
  if (!isAddress(addr)) throw _errors.UsageError.invalidAddress(addr);
}

function validateDouble(n) {
  if (!(0, _util.isDouble)(n)) throw (0, _errors.UsageError)('expected a number, got ' + n);
}

module.exports = {
  validateAddress: validateAddress,
  validateDouble: validateDouble
};
//# sourceMappingURL=validation.js.map
