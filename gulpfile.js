"use strict";

var gulp = require('gulp'),
  path = require('path'),
  fs = require('fs'),
  join = path.join,
  _ = require('lodash'),
  gutil = require('gulp-util'),
  runSequence = require('run-sequence'),
  mocha = require('gulp-mocha'),
  sourcemaps = require('gulp-sourcemaps'),
  babel = require('gulp-babel'),
  jshint = require('gulp-jshint'),
  spawnSync = require('child_process').spawnSync,
  clone = require('clone'),
  map = require('async').map;

var jshintConfig = {};

require.extensions['.json'](jshintConfig, './.jshintrc');

var jshintServerConfig = jshintConfig.exports;
var jshintClientConfig = clone(jshintServerConfig);

jshintClientConfig.predef = jshintClientConfig.predef.client;
jshintServerConfig.predef = jshintServerConfig.predef.server;

var packageJson = require('./package');

gulp.task('default', ['build-and-test']);

var reserved = ['default', 'test'];

var srcPath = 'src/**/*.js';

jshint.client = jshint.bind(null, jshintClientConfig);
jshint.server = jshint.bind(null, jshintServerConfig);

function buildExtTask(testFramework) {
  var task = `node ${path.join('node_modules', testFramework, 'bin', testFramework)}`;
  if (arguments.length > 2) {
    task += ' ';
    task += [].slice.call(arguments, 2).join(' ');
  }
  return task;
}

gulp.task('build:tasks', function () {
  packageJson.scripts = {};
  _.forOwn(gulp.tasks, function (value, key, obj) {
    if (~reserved.indexOf(key)) return;
    packageJson.scripts[key] = `node ${path.join('node_modules', 'gulp', 'bin', 'gulp')} ${key}`;
  });
  packageJson.scripts.test = buildExtTask('mocha');
  fs.writeFileSync('./package.json', JSON.stringify(packageJson, null, 1));
});

gulp.task('build-and-test', function (cb) {
  runSequence(['build', 'test'], cb);
});

gulp.task('watch', ['default'], function () {
  return gulp.watch(srcPath, ['default']);
});

gulp.task('build', ['build:app']);

gulp.task('build:app', function () {
  gulp.src('src/**/*.js')
    .pipe(sourcemaps.init())
    .pipe(babel({ presets: ['es2015'], plugins: ["transform-object-rest-spread"] }))
    .pipe(sourcemaps.write('.', { sourceRoot: join(__dirname, 'src') }))
    .pipe(gulp.dest('dist'));
});

gulp.task('jshint', ['jshint:app']);


gulp.task('jshint:app', function () {
  return gulp.src('./src/**/*.js')
    .pipe(jshint.server())
    .pipe(jshint.reporter('jshint-stylish'));
});

gulp.task('test', function () {
  return gulp.src('test/test.js', { read: false })
    .pipe(mocha({ reporter: 'nyan' }));
});
