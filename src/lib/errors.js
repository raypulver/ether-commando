"use strict";

import { ownAssign } from './util';

function UsageError(msg) {
  if (!(this instanceof UsageError)) return new UsageError(msg);
  const tmp = Error.call(this, msg);
  ownAssign(this, tmp);
}
UsageError.prototype = Object.create(Error.prototype);
UsageError.prototype.name = 'UsageError';
Object.assign(UsageError, {
  needAddressThenAmount() {
    return UsageError('must supply target address then amount');
  },
  invalidAddress(addr) {
    return UsageError(`invalid address ${addr}`);
  },
  nonexistentAccount(idx) {
    return UsageError(`nonexistent Ethereum account at index ${idx}`);
  }
});

module.exports = {
  UsageError
};
