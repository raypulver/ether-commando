"use strict";

import Getopt from 'node-getopt';
import { join, parse, relative } from 'path';
import { EOL, homedir } from 'os';
import { difference } from 'lodash';
import chalk from 'chalk';
import BN from 'big-number';
import { info, ln } from './log';
import { UsageError } from './errors';
import { validateAddress, validateDouble } from './validation';
import { isInteger, isDouble } from './util';
import { inspect } from 'util';
import { format } from 'url';
import pathExists from 'path-exists';
import mkdirp from 'mkdirp';
import { writeFileSync, readFileSync } from 'fs';
import Web3 from 'web3';
import log from './log';
import { mapSeries } from 'async';
import { range, zipObject, forOwn } from 'lodash';
import readlineSync from 'readline-sync';
import lightwallet from 'eth-lightwallet';
import HookedWeb3Provider from 'hooked-web3-provider';
import sprintf from 'sprintf';
import mathjs from 'mathjs';
import escape from 'escape-regexp';
import Transaction from 'ethereumjs-tx';
import { privateToPublic, publicToAddress } from 'ethereumjs-util';
import { install } from 'source-map-support';
import { fromV3, generate } from 'ethereumjs-wallet';

install();

const fmtStr = '  %-35s %-25s';

const fmt = sprintf.bind(null, fmtStr);

const existsSync = pathExists.sync;

const mkdirSync = mkdirp.sync;

const detached = new Web3();
const { toWei, fromWei } = detached;

const { eval: expr } = mathjs; // jshint ignore:line

const namespaceProps = ['contract', 'config', 'state'];

const { keys, assign, create } = Object;

const { yellow, green, red, magenta } = chalk;

function parseNamespace(data) {
  if (!data.contract) return null;
  return namespaceProps.reduce((r, v) => {
    r[v] = data[v];
    return r;
  }, {});
}

EtherCommando.log = log;

function statePath() {
  /* jshint validthis:true */
  return join(homePath.call(this), 'state.json');
} 

function homePath() {
  /* jshint validthis:true */
  return join(homedir(), `.${this._title}`);
}

const ConfigProto = {
  save() {
    /* jshint validthis:true */
    if (!existsSync(homePath.call(this._parent))) mkdirSync(homePath.call(this._parent));
    let parent = this._parent;
    delete this._parent;
    let retval = writeFileSync(statePath.call(parent), JSON.stringify(this, null, 1));
    this._parent = parent;
    return retval;
  }
};

function readOrCreateConfigObject() {
  /* jshint validthis:true */
  const link = { _parent: this };
  if (!existsSync(homePath.call(this))) mkdirSync(homePath.call(this));
  if (existsSync(statePath.call(this))) return assign(create(ConfigProto), assign(link, JSON.parse(readFileSync(statePath.call(this), 'utf8'))));
  else return assign(create(ConfigProto), link);
}

function bootstrap(config) {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    let root = parseNamespace(config);
    this._namespaces = assign({
      root: (root ? root : {})
    }, difference(keys(config), namespaceProps).reduce((r, v) => {
      r[v] = parseNamespace(config[v]);
      return r;
    }, {}));
    this._title = config.title;
    if (!this._title) this._title = parse(process.argv[1]).name;
    process.title = this._title;
    this._state = readOrCreateConfigObject.call(this);
    parseArgs.call(this);
    readSavedAddresses.call(this);
    bootstrapEthClient.call(this).then(() => {
      return parseGasAndPrice.call(this);
    }).then(() => {
      targetContract.call(this);
      selectUser.call(this);
      selectNamespace.call(this);
      this.action = buildAction.call(this);
      resolve();
    }).catch(reject);
  });
}

function selectNamespace() {
  /* jshint validthis:true */
  if (!this._namespaces[this.opts.argv[0]]) this._selectedNamespace = 'root';
  else this._selectedNamespace = this.opts.argv[0];
}

function callTarget(namespace, address) {
  /* jshint validthis:true */
  validateAddress(address);
  this._state[namespace] = address;
  this._state.save();
  info(`using ${green(address)} as ${red(namespace)}`);
}

function targetContract() {
  /* jshint validthis:true */
  let { dispatch } = this.opts.options;
  if (dispatch) {
    let signedTx = JSON.parse(readFileSync(dispatch, 'utf8'));
    return (this.addr = signedTx.addr);
  }
  if (this._namespaces[this.opts.argv[0]]) this.addr = this._namespaces[this.opts.argv[0]].address;
  else this.addr = this._namespaces.root.address;
  if (this.opts.options.addr) this.addr = aliasToAddr.call(this, this.opts.options.addr);
}

function callAlias(name, addr) {
  /* jshint validthis:true */
  addr = aliasToAddr.call(this, addr);
  validateAddress(addr);
  if (!this._state.aliases) this._state.aliases = {};
  this._state.aliases[name] = addr;
  info(`using ${green(addr)} as ${red(name)}`);
  this._state.save();
}

function selectUser() {
  /* jshint validthis:true */
  let { dispatch } = this.opts.options;
  if (dispatch) {
    let signedTx = JSON.parse(readFileSync(dispatch, 'utf8'));
    return (this.from = signedTx.owner);
  }
  this.from = this.accounts[0];
  if (this.opts.options.from) this.from = aliasToAddr.call(this, this.opts.options.from);
}

function aliasToAddr(alias) {
  /* jshint validthis:true */
  if (isInteger(alias)) {
    let accts;
    if (+alias >= 1e50) {
      alias = "0";
    }
    if ((alias = +alias) >= (accts = this.accounts).length) {
      throw UsageError.nonexistentAccount(alias);
    }
    return accts[alias];
  } else if (this._state[alias]) {
    return this._state[alias];
  } else if (this._state.aliases && this._state.aliases[alias]) {
    return this._state.aliases[alias];
  } else {
    return alias;
  }
}

function prompt(question) {
  return `${magenta('[prompt]')} ${question}`;
}

function subsTilde(str) {
  return str.replace(homedir(), '~');
}

function bootstrapEthClient() {
  /* jshint validthis:true */
  try {
    let hostname = this.opts.options.host || this._state.rpc && this._state.rpc.hostname || 'localhost';
    let { port, hd, password, unlock, out, local, dispatch, protocol, pathname } = this.opts.options;
    let passOverride = true;
    password = password || ((passOverride = false) || true) && this._state.rpc && this._state.rpc.password || null;
    let hdOverride = true;
    let localOverride = true;
    hd = hd || ((hdOverride = false) || true) && this._state.rpc && this._state.rpc.hd || null;
    unlock = unlock || this._state.rpc && this._state.rpc.unlock;
    local = local || ((localOverride = false) || true) && this._state.rpc && this._state.rpc.local || null;
    port = port || this._state.rpc && this._state.rpc.port || 8545; 
    pathname = this._state.rpc && this._state.rpc.pathname;
    protocol = this._state.rpc && this._state.rpc.protocol || 'http:';
    if (passOverride) hd = true;
    if (!hdOverride && localOverride) {
      hd = false;
      local = true;
    }
    if (hdOverride && !localOverride) {
      hd = true;
      local = false;
    }
    const url = format({
      hostname,
      port,
      pathname,
      protocol
    });
    if (port !== 8545 || hostname !== 'localhost' || protocol !== 'http:' || pathname) {
      if (!this._state.rpc) this._state.rpc = {};
      Object.assign(this._state.rpc, { port, hostname, protocol, pathname });
      this._state.save();
    }
    if (!this._state.rpc) this._state.rpc = {};
    Object.assign(this._state.rpc, {
      hd,
      unlock,
      password,
      local
    });
    this._state.save();
    if (!hd && !dispatch) return Promise.resolve((this.web3 = new Web3(new Web3.providers.HttpProvider(url))) && (this.accounts = this.web3.eth.accounts));
    let seed;
    let passwd;
    let rawKey;
    let absolute;
    let fmtPassword = () => {
      if (absolute) return relative(process.cwd(), password);
      return subsTilde(password);
    };
    if (password && !dispatch) {
      let parts = parse(password);
      if (!parts.dir) {
        let keyPath;
        password = join((keyPath = join(homePath.call(this), 'keystores')), password);
        if (!existsSync(keyPath)) mkdirSync(keyPath);
      } else absolute = true;
    }
    if ((!password || (out && password)) && !dispatch) {
      seed = readlineSync.question(prompt('HD seed: '));
      passwd = readlineSync.question(prompt('keystore password: '));
      writeFileSync(password, JSON.stringify({ seed, passwd }, null, 1));
      info(`keystore saved to ${green(fmtPassword())}`);
    } else if (!dispatch) {
      if (!existsSync(password)) {
        passwd = readlineSync.question(prompt('new keystore password: '));
        writeFileSync(password, JSON.stringify(generate().toV3(passwd, { n: 2048 }), null, 1));
        info(`keystore saved to ${green(fmtPassword())}`);
      } else {
        let saved = JSON.parse(readFileSync(password, 'utf8'));
        if (typeof saved === 'object') {
          if (saved.version === 3) {
            saved.crypto = saved.Crypto || saved.crypto;
            rawKey = fromV3(saved, readlineSync(prompt('keystore password: '))).getPrivateKey();
          } else {
            seed = saved.seed;
            passwd = saved.passwd;
            info(`using seed ${magenta(seed)}`);
            if (!passwd) passwd = readlineSync(prompt('keystore password: '));
          }
        } else {
          if (saved.substr(0, 2) === '0x') saved = saved.substr(2);
          rawKey = Buffer.from(saved, 'hex');
        }
      }
    }
    return new Promise((resolve, reject) => {
      if (dispatch) {
        let signedTx = JSON.parse(readFileSync(dispatch, 'utf8'));
        return resolve(this.web3 = new Web3(new HookedWeb3Provider({
          host: url,
          transaction_signer: {
            hasAddress: (addr, cb) => { cb(null, true); },
            signTransaction: (tx, cb) => { cb(null, signedTx.tx); }
          }
        })));
      }
      if (rawKey) {
        this.accounts = ['0x' + publicToAddress(privateToPublic(rawKey)).toString('hex')];
        return resolve(this.web3 = new Web3(new HookedWeb3Provider({
          host: url,
          transaction_signer: {
            hasAddress: (addr, cb) => {
              return cb(null, addr === this.accounts[0]);
            },
            signTransaction: (tx, cb) => {
              let o = new Transaction(tx);
              o.sign(rawKey);
              return cb(null, '0x' + o.serialize().toString('hex'));
            }
          }
        })));
      }
      lightwallet.keystore.createVault({
        seedPhrase: seed,
        password: passwd
      }, (err, transaction_signer) => {
        this.keystore = transaction_signer;
        if (err) return reject(err);
        transaction_signer.keyFromPassword(passwd, (err, result) => {
          if (err) return reject(err);
          transaction_signer.passwordProvider = (cb) => { return cb(null, passwd); };
          this.pwDerivedKey = result;
          transaction_signer.generateNewAddress(result, unlock || 12);
          this.accounts = transaction_signer.getAddresses().map((v) => {
            if (v.substr(0, 2) !== '0x') return '0x' + v;
            return v;
          });
          let { sign } = this.opts.options;
          if (sign) this.web3 = new Web3(new HookedWeb3Provider({
            host: url,
            transaction_signer: {
              hasAddress: (addr, cb) => {
                return transaction_signer.hasAddress(addr, cb);
              },
              signTransaction: (txParams, cb) => {
                let retval = transaction_signer.signTransaction(txParams, (err, signed) => {
                  writeFileSync(sign, JSON.stringify({
                    tx: signed,
                    date: new Date(),
                    owner: this.from,
                    namespace: this.namespaceName,
                    call: this.func,
                    args: this.args,
                    addr: this.addr
                  }, null, 1));
                  info('transaction saved to ' + green(sign));
                  process.exit(0);
                });
                return retval;
              }
            }
          }));
          else this.web3 = new Web3(new HookedWeb3Provider({
            host: url,
            transaction_signer
          }));
          resolve(this.web3);
        });
      });
    });
  } catch (e) {
    return Promise.reject(e);
  }
}

function readSavedAddresses() {
  /* jshint validthis:true */
  keys(this._state).forEach((v) => {
    if (v === 'rpc' || v === 'aliases') return;
    let realKey = v;
    if (v === this._title) realKey = 'root';
    if (!this._namespaces[realKey]) this._namespaces[realKey] = {};
    this._namespaces[realKey].address = this._state[v];
    if (!this._namespaces[realKey].config) this._namespaces[realKey].config = {};
    if (this.opts.addr) this._namespaces[realKey].address = this.opts.addr;
  });
  this._aliases = this._state.aliases || {};
}

const cmds = [
  ['help', 'display available namespaces and root contract'],
  ['[namespace] help', 'display functions in [namespace]'],
  ['sendEther [account] [amount]', 'send [amount] ETH to [account]'],
  ['info', 'output root namespace address and account used'],
  ['[namespace] info', 'output address for [namespace]'],
  ['target [address]', 'make [address] default address for root contract'],
  ['[namespace] target [address]', 'make [address] default address for [namespace]'],
  ['[namespace] siphon [filename]', 'download contract state into [filename]'],
  ['[namespace] upload [filename]', 'upload state from [filename] to [namespace]'],
  ['etherBalanceOf [address]', 'check ETH balance at [address]'],
  ['deploy [-t]', 'deploy root contract'],
  ['[namespace] deploy [-t]', 'deploy [namespace] contract'],
  ['alias [name] [address]', 'set alias']
];

function parseArgs() {
  /* jshint validthis:true */
  let getopt = new Getopt([
    ['h', 'help', 'display this help'],
    ['q', 'no-event', 'do not wait for transaction to be mined'],
    ['r', 'price=GASPRICE', 'gasPrice value (saves)'],
    ['v', 'version', 'display version number'],
    ['p', 'port=PORT', 'JSON-RPC port number (saves)'],
    ['g', 'gas=GAS', 'maximum quantity of gas (saves)'],
    ['a', 'addr=ADDRESS', 'contract address'],
    ['j', 'host=HOST', 'JSON-RPC hostname (saves)'],
    ['t', 'target', 'target new contract automatically after deploy'],
    ['f', 'from=ADDRESS', 'Ethereum address to send transactions from'],
    ['d', 'hd', 'enable HD wallet (saves)'],
    ['l', 'local', 'use local accounts on RPC (saves)'],
    ['n', 'unlock=NUMACCOUNTS', 'number of HD wallets to unlock (saves)'],
    ['x', 'password=PASSWORDFILE', 'supply path to HD seed/password (saves)'],
    ['o', 'out', 'force prompt for HD credentials to save'],
    ['s', 'sign=FILE', 'sign transaction and save to file'],
    ['u', 'dispatch=FILE', 'dispatch a saved transaction']
  ]);
  getopt.setHelp(`${yellow('[help]')} ${grayBold(`${this._title} [-hvtdo] [-p PORT] [-g GAS] [-a ADDR] [-f FROM] [-n NUM] [-x FILE] [NAMESPACE] COMMAND`)}${EOL}${yellow('commands:')}${EOL}${cmds.map((v) => { return grayBold(fmt.apply(null, v)); }).join(EOL)}${EOL}${yellow('options:')}${EOL}${grayBold('[[OPTIONS]]')}`);
  getopt.errorFunc = (e) => { this._error = e; };
  this.opts = getopt.parseSystem();
  if (this.opts.options.help) {
    getopt.showHelp();
    process.exit(0);
  }
}

function reportGasPrices(gas, gasPrice) {
  if (!gas) gas = 'default';
  else gas = (+gas).toExponential();
  if (!gasPrice) gasPrice = 'default';
  else gasPrice = (+gasPrice).toExponential() + ' wei';
  info(`posting ${yellow(gas)} gas with gasPrice of ${yellow(gasPrice)}`);
}

const doesntRequireTarget = ['deploy', 'target', 'sendEther', 'etherBalanceOf', 'alias'];

function buildAction() {
  /* jshint validthis:true */
  let namespace = this._namespaces.root, namespaceName = 'root', func, args, funcObj, contract, Contract;
  let { dispatch } = this.opts.options;
  return () => {
    if (this._error) return Promise.reject(this._error);
    try {
      if (dispatch) {
        let signedTx = JSON.parse(readFileSync(dispatch, 'utf8'));
        namespaceName = signedTx.namespace;
        namespace = this._namespaces[namespaceName];
        func = this.func = signedTx.call;
        args = this.args = signedTx.args;
      } else {
        if (this._namespaces[this.opts.argv[0]]) {
          namespace = this._namespaces[this.opts.argv[0]];
          namespaceName = this.namespaceName = this.opts.argv[0];
          func = this.func = this.opts.argv[1];
          args = this.args = this.opts.argv.slice(2);
        } else {
          namespaceName = this.namespaceName = this._title || namespace.name;
          func = this.func = this.opts.argv[0];
          args = this.args = this.opts.argv.slice(1);
        }
      }
      if (!func) return callInfo.call(this, this.from, this.addr, namespaceName);
      switch (func) {
        case 'target':
          return Promise.resolve(callTarget.call(this, namespaceName, args[0]));
        case 'alias':
          return Promise.resolve(callAlias.call(this, args[0], args[1]));
        case 'sendEther':
          return sendEther.call(this);
        case 'etherBalanceOf':
          return etherBalanceOf.call(this);
        case 'getGasPrice':
          return getGasPrice.call(this, args[0]);
        case 'getTransactionReceipt':
          return getTransactionReceipt.call(this, args[0]);
        case 'sign':
          return sign.apply(this, args);
        case 'getGasLimit':
          return getGasLimit.call(this, args[0]);
        case 'getBlock':
          return getBlock.call(this, args[0], args[1]);
        case 'getBlockNumber':
          return getBlockNumber.call(this);
        case 'getSyncing':
          return getSyncing.call(this);
      }
      Contract = this.Contract = namespace.contract && this.web3.eth.contract(namespace.contract.abi) || {};
      switch (func) {
        case 'help':
          return help.call(this, namespaceName, namespace.contract && namespace.contract.abi);
        case 'deploy':
          return deploy.call(this, namespaceName, namespace, args);
        case 'info':
          return callInfo.call(this, this.from, this.addr, namespaceName);
      }
      contract = this.contract = Contract.at(this.addr);
      switch (func) {
        case 'siphon':
          return siphon.call(this, namespaceName, namespace, args[0]);
        case 'upload':
          return upload.call(this, namespace, args[0]);
      }
      if (!(funcObj = namespace.contract.abi.find((v) => { return v.name === func; }))) return Promise.reject(UsageError(`function/namespace ${func} not found`));
 
      reportAccount(this.from);
      if (!~doesntRequireTarget.indexOf(func) && this.addr) info(`using ${green(this.addr)} for ${red(namespaceName)}`);
      else if (!this.addr) return Promise.reject(UsageError(`must supply address for ${namespaceName}`));
    } catch (e) {
      return Promise.reject(e);
    }
    return new Promise((resolve, reject) => {
      args.forEach((v, i, arr) => {
        if (funcObj.inputs[i]) {
          switch (funcObj.inputs[i].type) {
            case 'address':
              arr[i] = aliasToAddr.call(this, v);
              break;
          }
        }
      });
      let promise = namespace.config && namespace.config[func] && namespace.config[func].transformArgs && namespace.config[func].transformArgs.call(this, args) || Promise.resolve(args);
      promise.then((args) => {
        args = convertArgs.call(this, args, funcObj);
        let argStr = argString.call(this, args, funcObj);
        if (funcObj.constant || namespace.config && namespace.config[func] && namespace.config[func].constant) {
          info(`calling ${green(func)} on ${red(namespaceName)}${args.length ? ` with argument${args.length > 1 ? 's' : ''} ${argStr}` : ''}`);
          let { from } = this;
          args.push({ from });
          args.push((() => {
            let afterFirst = false;
            return (err, result) => {
              let thisRun = afterFirst;
              afterFirst = true; 
              if (!thisRun) {
                if (err) return reject(err);
                if (namespace.config && namespace.config[func] && namespace.config[func].hook) {
                  namespace.config[func].hook.call(this, result, resolve, reject);
                } else {
                  ln(String(result));
                  resolve(result);
                }
              }
            };
          })());
          contract[func].call.apply(contract[func], args);
        } else {
          info(`transacting ${green(func)} with ${red(namespaceName)}${args.length ? ` using argument${args.length > 1 ? 's' : ''} ${argStr}` : ''}`);
          const { from, gas, gasPrice } = this;
          let txCfg = { from, gas, gasPrice };
          if (!gas) delete txCfg.gas;
          if (!gasPrice) delete txCfg.gasPrice;
          reportGasPrices(gas, gasPrice);
          args.push(txCfg);
          const noEvent = this.opts.options['no-event'];
          
          if (namespace.config && namespace.config[func] && namespace.config[func].hook && !noEvent) {
            args.push((err, result) => {
              if (err) return reject(err);
              reportTx.call(this, result);
              namespace.config[func].hook.call(this, result, resolve, reject);
            });
          } else if (noEvent || namespace.config  && namespace.config[func] && namespace.config[func].noEvent) {
            args.push((err, result) => {
              if (err) return reject(err);
              reportTx.call(this, result);
              pollForMinedReceipt.call(this, result, resolve);
            });
          } else {
            args.push((err, tx) => {
              if (err) return reject(err);
              reportTx.call(this, tx);
              let filter = this.web3.eth.filter({
                address: this.addr,
                topics: []
              });
              info('waiting for transaction to be mined...');
              filter.watch((err, result) => {
                if (err) {
                  filter.stopWatching();
                  return reject(err);
                }
                if (result.transactionHash === tx) {
                  filter.stopWatching();
                  info('transaction mined!');
                  return this.web3.eth.getTransactionReceipt(tx, (err, result) => {
                    if (err) return reject(err);
                    info(`gas used: ${yellow(result.cumulativeGasUsed)}`);
                    resolve(tx);
                  });
                }
              });
            });
          }
        }
        contract[func].apply(contract[func], args);
      }).catch((err) => { reject(err); });
    });
  };
}
function logHelp(msg) {
  return ln(`${yellow('[help]')} ${msg}`);
}

function reportAccount(acct) {
  if (!acct) return info('no account selected');
  return info(`using account ${green(acct)}`);
}

function sign() {
  /* jshint validthis:true */
  const { from } = this;
  const args = [from].concat([].slice.call(arguments));
  if (this.keystore) return new Promise((resolve, reject) => {
    args.splice(0, 1);
    const sig = lightwallet.signing.concatSig(lightwallet.signing.signMsgHash(this.keystore, this.pwDerivedKey, this.web3.sha3.apply(this.web3, args), this.from));
    return resolve(sig);
  });
  return new Promise((resolve, reject) => {
    args.push((err, result) => {
      if (err) return reject(err);
      info(result);
      resolve(result);
    });
    console.log(require('util').inspect(args));
    this.web3.eth.sign.apply(this.web3.eth, args);
  });
}

function reportRPC() {
  /* jshint validthis:true */
  const rpcURI = getRPCURI.call(this);
  if (!rpcURI) info('no RPC selected');
  info(`using RPC at ${green(rpcURI)}`);
}

function callInfo(from, addr, namespaceName) {
  /* jshint validthis:true */
  reportRPC.call(this);
  reportAccount(from);
  if (addr) info(`using ${green(addr)} for ${red(namespaceName)}`);
  return Promise.resolve();
}

function grayBold(d) {
  return chalk.gray(chalk.bold(d));
}

function help(namespaceName, abi) {
  /* jshint validthis:true */
  if (namespaceName === this._title) {
    logHelp('available help categories:');
    ln(Object.keys(this._namespaces).filter((v) => {
      switch (v) {
        case 'root':
        case '_parent':
          return false;
        default:
          return true;
      }
    }).map((v) => {
      return `  ${grayBold(v)}`;
    }).join(EOL));
  }
  if (abi) {
    logHelp(`available functions in namespace ${red(namespaceName)}`);
    abi = abi.slice().filter((v) => {
      return v.type === 'function';
    });
    abi.sort((a, b) => {
      return a.name > b.name;
    });
    ln(abi.map((v) => {
      return `  ${grayBold(v.name)}${v.inputs && v.inputs.length && ' ' || ''}${v.inputs && v.inputs.map((v) => {
        return grayBold(`[${v.type}]`);
      }).join(' ') || ''}`;
    }).join(EOL));
  }
}

function deleteNullKeys(obj) {
  keys(obj).forEach((v) => {
    if (typeof obj[v] === 'object') deleteNullKeysHelper(obj[v]);
  });
}

function deleteNullKeysHelper(obj) {
  if (!Array.isArray(obj)) {
    forOwn(obj, function (value, key, obj) {
      if (typeof value === 'object') {
        deleteNullKeys(value);
        if (!keys(value).length) delete obj[key];
      } else if (value === 0 || value === false) delete obj[key];
    });
  } else {
    obj.forEach((v) => {
      if (typeof value === 'object') deleteNullKeys(v);
    });
  }
}
  
function siphon(namespaceName, namespace, filename) {
  return new Promise((resolve, reject) => {
    if (!filename) filename = `${namespaceName}-${String(Date.now())}.json`;
    if (!namespace.state) throw UsageError('no state defined for contract');
    let state = {}, cfg = namespace.state, ref = cfg, ks;
    mapSeries((ks = keys(cfg)), (v, next) => {
      info(`siphoning ${v}`);
      ref = cfg[v];
      let get = ref.get = ref.get || v;
      let path = [];
      while (ref.indexedBy) {
        path.push({
          get: ref.indexedBy.get,
          length: ref.indexedBy.length,
          ref
        });
        ref = ref.indexedBy;
      }
      if (!path.length) {
        return this.contract[ref.get].call((err, result) => {
          if (err) return next(err);
          if (typeof result.lessThan === 'function') result = +result;
          next(null, result);
        });
      }
      path.reverse();
      const self = this;
      (function siphonMapping(seg, path, i, keys, cb) {
        if (!keys) keys = [];
        keys.push((err, result) => {
          if (err) return cb(err);
          if (typeof result.lessThan === 'function') result = +result;
          keys.pop();
          mapSeries(range(result), (v, next) => {
            keys.push(v);
            keys.push((err, result) => {
              if (typeof result.lessThan === 'function') result = +result;
              keys.pop();
              keys.pop();
              next(err, result);
            });
            self.contract[seg.get].call.apply(self.contract[seg.get], keys);
          }, (err, result) => {
            if (err) return cb(err);
            if (path.length !== i + 1) {
              mapSeries(result, (v, next) => {
                keys.push(v);
                siphonMapping(path[i + 1], path, i + 1, keys, (err, results) => {
                  keys.pop();
                  next(err, results.reduce((r, v, i) => {
                    r.push([result[i]].concat(v));
                    return r;
                  }, []));
                });
              }, (err, results) => {
                if (err) return cb(err);
                return cb(null, results.reduce((r, v) => {
                  return r.concat(v);
                }, []));
              });
            } else cb(null, result.map((v) => {
              return [v];
            }));
          });
        });
        self.contract[seg.length].call.apply(self.contract[seg.length], keys);
      })(path[0], path, 0, [], next);
    }, (err, results) => {
      if (err) return reject(err);
      let i = -1;
      mapSeries(results, (v, next) => {
        i++;
        if (!Array.isArray(v)) return next(null, v);
        mapSeries(v, (w, next) => {
          w.push((err, result) => {
            if (typeof result.lessThan === 'function') result = +result;
            w.pop();
            next(err, result);
          });
          this.contract[cfg[ks[i]].get].call.apply(this.contract[cfg[ks[i]].get], w);
        }, next);
      }, (err, result) => {
        result = zipObject(ks, result.map((v, i) => {
          if (!Array.isArray(v)) return v;
          return v.reduce((r, v, idx) => {
            let ref = r, last;
            results[i][idx].forEach((v, i, arr) => {
              if (i === arr.length - 1) return (last = v);
              if (!ref[v]) {
                ref[v] = {};
                ref = ref[v];
              }
            });
            ref[last] = v;
            return r;
          }, {});
        }));
        deleteNullKeys(result);
        writeFileSync(filename, JSON.stringify(result, null, 1));
        info(`${red(namespaceName)} saved to ${green(filename)}`);
        resolve(result);
      });
    });
  });
}

function objectTo3DArray(obj) {
  return keys(obj).map((v) => {
    return [v, objectTo3DArrayHelper(obj[v])];
  });
}

function objectTo3DArrayHelper(obj) {
  if (typeof obj === 'object') return keys(obj).reduce((r, v) => {
    return r.concat(objectTo3DArrayHelper(obj[v]).map((i) => {
      i.unshift(v);
      return i;
    }));
  }, []);
  return [[obj]];
}

function upload(namespace, filename) {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    const { from, gas, gasPrice } = this;
    let txCfg = { from, gas, gasPrice };
    if (!gas) delete txCfg.gas;
    if (!gasPrice) delete txCfg.gasPrice;
    let cfg = namespace.state, ref;
    let obj = JSON.parse(readFileSync(filename, 'utf8'));
    deleteNullKeys(obj);
    if (!cfg) throw UsageError('no state defined for contract');
    mapSeries(ln(objectTo3DArray(obj)), (v, next) => {
      info(`uploading ${v[0]}`);
      mapSeries(v[1], (w, next) => {
        if (!Array.isArray(w)) w = [w];
        w.push(txCfg);
        w.push(next);
        this.contract[cfg[v[0]].set].sendTransaction.apply(this.contract[cfg[v[0]].set], w);
      }, next);
    }, (err, result) => {
      if (err) return reject(err);
      info('done!');
      resolve();
    });
  });
}

function sendEther() {
  /* jshint validthis:true */
  this.opts.argv[1] = aliasToAddr.call(this, this.opts.argv[1]);
  validateAddress(this.opts.argv[1]);
  validateDouble(this.opts.argv[2]);
  info(`sending ${red(this.opts.argv[2])} ether to ${green(this.opts.argv[1])}`);
  let { from, gas, gasPrice } = this;
  let [ _, to, value ] = this.opts.argv;
  value = toWei(+value, "ether");
  let txCfg = { from, gas, gasPrice, to, value };
  reportGasPrices(gas, gasPrice);
  if (!gas) delete txCfg.gas;
  if (!gasPrice) delete txCfg.gasPrice;
  return new Promise((resolve, reject) => {
    this.web3.eth.sendTransaction(txCfg, (err, tx) => {
      if (err) return reject(err);
      info(`created transaction at ${green(tx)}`);
      pollForMinedReceipt.call(this, tx, resolve);
    });
  });
}

function defer() {
  let resolve, reject, promise = new Promise((_resolve, _reject) => {
    resolve = _resolve;
    reject = _reject;
  });
  return {
    promise,
    resolve,
    reject
  };
}

function timeout(n) {
  let timer = null, deferred = defer();
  const cancel = () => {
    if (timer) {
      deferred.reject(Error('@@interrupt'));
      clearTimeout(timer);
    }
  };
  timer = setTimeout(() => {
    deferred.resolve();
    timer = null;
  }, n);
  return {
    cancel,
    promise: deferred.promise
  };
}

const POLL_INTERVAL = 5000;

function dispatchPoll(tx, resolve, cancel) {
  /* jshint validthis:true */
  const { promise } = timeout(POLL_INTERVAL);
  return promise.then(() => {
    return pollOne.call(this, tx, resolve, cancel);
  });
}

function pollOne(tx, resolve, cancel) {
  /* jshint validthis:true */
  let receipt;
  return getTransactionReceiptQuiet.call(this, tx).then((_receipt) => {
    receipt = _receipt;
    return getBlockNumberQuiet.call(this);
  }).then((number) => {
    if (receipt && (receipt.logs && receipt.logs.find(v => v.type === 'mined') || receipt.blockNumber >= number)) {
      cancel();
      info('transaction mined!');
      return resolve(receipt);
    }
    return dispatchPoll.call(this, tx, resolve, cancel);
  });
}

function pollForMinedReceipt(tx, resolve) {
  /* jshint validthis:true */
  const { cancel, promise } = timeout(3000);
  promise.then(() => info('waiting for transaction to be mined...')).catch((err) => err);
  return pollOne.call(this, tx, resolve, cancel);
}

function deploy(namespaceName, namespace, args) {
  /* jshint validthis:true */
  let contract = namespace.contract;
  let cons = contract.abi.find((v) => {
    return v.type === 'constructor';
  });
  let defArgs = (cons && cons.inputs || []).map((v) => {
    if (/uint/.test(v.type)) return 0;
    if (/string/.test(v.type)) return "";
    if (v.type === 'bool') return false;
    return "0x0";
  });
  args.forEach((v, i) => {
    if ((cons.inputs[i] || {}).type === 'address') defArgs[i] = aliasToAddr.call(this, v);
    else if (/uint/.test((cons.inputs[i] || {}).type)) defArgs[i] = +v;
    else defArgs[i] = v;
  });
  args = defArgs;
  const { from, gas, gasPrice } = this;
  const data = contract.binary;
  let txCfg = { from, gas, gasPrice, data };
  reportGasPrices(gas, gasPrice);
  if (!gas) delete txCfg.gas;
  if (!gasPrice) delete txCfg.gasPrice;
  args.push(txCfg);
  return new Promise((resolve, reject) => {
    let timer;
    args.push((err, result) => {
      if (err) return reject(err);
      if (!result.address) {
        pollForMinedReceipt.call(this, result.transactionHash, resolve);
      } else {
        if (timer) clearTimeout(timer);
        info(`${red(namespaceName)} deployed to ${green(result.address)}`);
        let { address } = result;
        if (result.transactionHash) this.web3.eth.getTransactionReceipt(result.transactionHash, (err, result) => {
          info(`gas used: ${yellow(result.cumulativeGasUsed)}`);
          resolve(address);
        });
      }
    });
    this.Contract.new.apply(this.Contract, args);
  }).then((receipt) => {
    return Promise.resolve(callTarget.call(this, namespaceName, receipt.contract));
  });
}

function etherBalanceOf() {
  /* jshint validthis:true */
  this.opts.argv[1] = aliasToAddr.call(this, this.opts.argv[1]);
  validateAddress(this.opts.argv[1]);
  return new Promise((resolve, reject) => {
    let balance = this.web3.eth.getBalance(this.opts.argv[1]);
    info(`current balance: ${green(String(fromWei(balance, "ether")) + ' ETH')}`);
    resolve(balance);
  });
}

function reportTx(result) { 
  /* jshint validthis:true */
  info(`transaction available at ${green(result)}`);
  const { _state: state } = this;
  state.data = state.data || {};
  state.data.lastTx = result;
  state.save();
}

function keyToRegExp(key) {
  return RegExp(escape(key), 'ig');
}

function exprSub(exp, vals) {
  let ks = Object.keys(vals);
  ks.sort((a, b) => {
    return a.length < b.length;
  });
  ks.forEach((k) => {
    exp = exp.replace(keyToRegExp(k), String(vals[k]));
  });
  return expr(exp);
}

const unitMap = {
  'wei': 1,
  'kwei': 1000,
  'ada': 1000,
  'femtoether': 1000,
  'mwei': 1000000,
  'babbage': 1000000,
  'picoether': 1000000,
  'gwei': 1000000000,
  'shannon': 1000000000,
  'nanoether': 1000000000,
  'nano': 1000000000,
  'szabo': 1000000000000,
  'microether': 1000000000000,
  'micro': 1000000000000,
  'finney': 1000000000000000,
  'milliether': 1000000000000000,
  'milli': 1000000000000000,
  'ether': 1000000000000000000,
  'eth': 1000000000000000000,
  'kether': 1000000000000000000000,
  'grand': 1000000000000000000000,
  'einstein': 1000000000000000000000,
  'mether': 1000000000000000000000000,
  'gether': 1000000000000000000000000000,
  'tether': 1000000000000000000000000000000
};

function mapOwn(obj, cb, thisArg) {
  let retval = Object.create(Object.getPrototypeOf(obj));
  forOwn(obj, (value, key) => {
    retval[key] = cb.call(thisArg, value, key, obj);
  });
  return retval;
}

function exprSubCurrencies(exp, vals) {
  Object.assign(vals, mapOwn(unitMap, (value) => {
    return '*' + String(value);
  }));
  return exprSub(exp, vals);
}

const txHashRe = /0x[a-f0-9]{32}/i;

function getTransactionReceiptQuiet(hash) {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    const { data } = this._state;
    if (hash === 'last') hash = data && data.lastTx;
    if (!txHashRe.test(hash)) throw Error(`invalid transaction hash: ${hash}`);
    this.web3.eth.getTransactionReceipt(hash, (err, result) => {
      if (err) return reject(err);
      resolve(result);
    });
  });
}

function getTransactionReceipt(hash) {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    const { data } = this._state;
    if (hash === 'last') hash = data && data.lastTx;
    if (!txHashRe.test(hash)) throw Error(`invalid transaction hash: ${hash}`);
    info(`getting transaction receipt for ${hash}`);
    this.web3.eth.getTransactionReceipt(hash, (err, result) => {
      if (err) return reject(err);
      ln(colorInspect(result));
    });
  });
}

const colorsTrue = { colors: true };

function colorInspect(data) {
  return inspect(data, colorsTrue);
}

const trueRe = /_/.test.bind(/^true$/i);

function stringToBool(val) {
  if (trueRe(String(val))) return true;
  return false;
}

function getBlockNumber() {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    this.web3.eth.getBlockNumber((err, result) => {
      if (err) return reject(err);
      info(`current block number: ${colorInspect(+result)}`);
    });
  });
}

function getBlockNumberQuiet() {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    this.web3.eth.getBlockNumber((err, result) => {
      if (err) return reject(err);
      resolve(result);
    });
  });
}

function getBlock(arg, transactionObjects) {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    arg = !isNaN(arg) && +arg || arg;
    transactionObjects = stringToBool(transactionObjects);
    info(`getting block ${colorInspect(arg)}`);
    this.web3.eth.getBlock(arg, transactionObjects, (err, result) => {
      if (err) return reject(err);
      ln(colorInspect(result));
    });
  });
}

function getSyncing() {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    info('testing if Ethereum node is syncing');
    this.web3.eth.getSyncing((err, result) => {
      if (err) return reject(err);
      ln(colorInspect(result));
    });
  });
}

function getRPCURI() {
  /* jshint validthis:true */
  const { rpc } = this._state;
  if (!rpc || !rpc.hostname) return '';
  const { hostname, port } = rpc;
  return format({
    protocol: 'http:',
    hostname,
    port
  });
}
  

const { log: mathLog, floor, pow } = Math;

const pow10 = pow.bind(null, 10);

function zeroes(v) {
  v = +v;
  return floor(mathLog(v)/mathLog(10));
}

function numToDenom(v, precision) {
  v = +v;
  const numZeroes = zeroes(v);
  const multiple = floor(numZeroes/3)*3;
  const unit = keys(unitMap).find((v) => {
    return zeroes(unitMap[v]) === multiple;
  }) || 'wei';
  let amt = v/pow10(multiple);
  if (precision === void 0) precision = 3;
  amt = floor(amt*pow10(precision))/pow10(precision);
  return `${amt} ${unit}`;
}
 
function getGasPrice() {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    this.web3.eth.getGasPrice((err, result) => {
      if (err) return reject(err);
      result = +result;
      info(`gas price: ${green(numToDenom(result))}`);
      resolve(result);
    });
  });
}

function getGasLimit() {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    this.web3.eth.getBlock('latest', (err, block) => {
      if (err) return reject(err);
      info(`gas limit: ${green(block.gasLimit)}`);
      resolve(block.gasLimit);
    });
  });
}

function parseGasAndPrice() {
  /* jshint validthis:true */
  return new Promise((resolve, reject) => {
    let { price: gasPrice, gas } = this.opts.options;
    let { price: originalGasPrice, gas: originalGas } = this.opts.options;
    if (!this._state.rpc) this._state.rpc = {};
    const { gasPrice: savedGasPrice, gas: savedGas } = this._state.rpc;
    Promise.all([
      new Promise((res, rej) => {
        this.web3.eth.getBlock('latest', (err, block) => {
          if (err) return rej(err);
          res(block);
        });
      }),
      new Promise((res, rej) => {
        this.web3.eth.getGasPrice((err, result) => {
          if (err) return rej(err);
          res(+result);
        });
      })
    ]).then((results) => {
      const [ block, currentGasPrice ] = results;
      if (!gasPrice && savedGasPrice) {
        gasPrice = savedGasPrice;
        originalGasPrice = savedGasPrice;
      }
      if (!gas && savedGas) {
        gas = savedGas;
        originalGas = savedGas;
      }
      if (gasPrice && gasPrice !== 'default' && isNaN(gasPrice)) gasPrice = exprSubCurrencies(gasPrice, { current: currentGasPrice });
      if (gas && gas !== 'default' && isNaN(gas)) gas = exprSub(gas, { max: block.gasLimit });
      if (gas === 'default') gas = void 0;
      else gas = +gas;
      if (gasPrice === 'default') gasPrice = void 0;
      else gasPrice = +gasPrice;
      const gasProps = { gas: originalGas, gasPrice: originalGasPrice };
      Object.assign(this, { gas, gasPrice });
      if (!this._state.rpc) this._state.rpc = {};
      Object.assign(this._state.rpc, gasProps);
      this._state.save();
      resolve();
    }).catch((err) => { reject(err); });
  });
}


function convertArgs(args, abiDesc) {
  /* jshint validthis:true */
  if (args.length !== abiDesc.inputs.length) throw UsageError(`function expects ${abiDesc.inputs.length} arguments, ${args.length} provided`);
  return args.map((v, i) => {
    if (abiDesc.inputs[i].type === 'address') return aliasToAddr.call(this, v);
    if (/uint/.test(abiDesc.inputs[i].type)) return +v;
    if (abiDesc.inputs[i].type === 'bool') {
      if (v === 'true') return true;
      return false;
    }
    return v;
  });
}

function argString(args, abiDesc) {
  let compare;
  return args.map((v, i) => {
    switch ((compare = abiDesc.inputs[i].type)) {
      case 'bool':
        return chalk.yellow(String(v));
      case 'address':
        return chalk.green(v);
      default:
        if (/uint/.test(v)) return chalk.red(v);
        return String(v);
    }
  }).join(', ');
}
    
function EtherCommando(config = {}) {
  if (!(this instanceof EtherCommando)) return new EtherCommando(config);
  return bootstrap.call(this, config).then(() => {
    return this.action();
  });
} 

EtherCommando.prototype = {
  callTarget,
  pollForMinedReceipt
};

module.exports = EtherCommando;
