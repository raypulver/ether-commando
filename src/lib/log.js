"use strict";

import chalk from 'chalk';

const { log, error: cerror } = console;

module.exports = {
  info(v) {
    log(`${chalk.cyan('[info]')} ${v}`);
    return v;
  },
  warning(v) {
    log(`${chalk.yellow('[warning]')} ${v}`);
    return v;
  },
  ln(v) {
    log(v);
    return v;
  },
  error(v) {
    cerror(`${chalk.red('[error]')} ${v}`);
    return v;
  }
};
