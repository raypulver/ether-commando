"use strict";

function ownAssign(dest, src) {
  Object.getOwnPropertyNames(src).forEach((v) => {
    Object.defineProperty(dest, v, Object.getOwnPropertyDescriptor(src, v));
  });
  return dest;
}

function isInteger(n) {
  if (typeof n === 'number' && n % 1 === 0 || /^\d+$/.test(n)) return true;
  return false;
}

function isDouble(n) {
  if (typeof n === 'number' || /^\d+(\.\d+)?$/.test(n)) return true;
}

module.exports = {
  ownAssign,
  isInteger,
  isDouble
};
