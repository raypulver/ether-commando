"use strict";

import { UsageError } from './errors';
import Web3 from 'web3';
import { isDouble } from './util';

const detached = new Web3();

const { isAddress } = detached;

function validateAddress(addr) {
  if (!isAddress(addr)) throw UsageError.invalidAddress(addr);
}

function validateDouble(n) {
  if (!isDouble(n)) throw UsageError(`expected a number, got ${n}`);
}

module.exports = {
  validateAddress,
  validateDouble
};
